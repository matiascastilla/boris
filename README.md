# Examen Mercado Libre


## Boris, pronóstico del clima para galaxias remotas

“Boris has just given me a summary of his views. He is a weather prophet. The weather will continue bad, he says. [...] The weather will not change”.

― Henry Miller, Tropic of Cancer 


### Descripción de la solución

En primer lugar desarrollé un prototipo en Pharo Smalltalk para explorar el problema. Este prototipo incluye una animación gráfica del sistema solar.
Luego desarrollé una versión "de producción" en Python. El objetivo de esta versión fue mostrar lo que considero buenas prácticas de Ingeniería de Software. Al tratarse de un sistema chico y ficticio, puede dar la impresión de que está sobrediseñado (overengineered). En un sistema real, hasta qué punto se aplican estas prácticas depende de la escala del proyecto y de los tradeoffs específicos que pueda imponer el contexto.

### Prototipo

El protitpo está desarrollado en Pharo Smalltalk. La imagen (el programa) se encuentra comprimida en `prototype/boris-pharo.zip`.
Para abrirla hay que tener instalada la vm de Pharo 5: http://pharo.org/download.
Una vez abierta la imagen, ejecutar en el Playground:
```smalltalk
SolarSystemSample run.
```
![alt text](doc/solar_system.png "Sistema Solar")


### Version Python

#### Instalación

Usando PIP:
```
pip install git+https://github.com/mcubik/boris.git@master
```

#### Generación del resumen del periodo
Una vez instalada la aplicación, se puede usar el siguiente comando para generar el resumen de los próximos diez años :
```bash
galaxyml summary 10
```

### API REST

La API rest se instaló en la plataforma Heroku. La url del servicio es https://warm-cove-88102.herokuapp.com
```
curl https://warm-cove-88102.herokuapp.com/clima?dia=566
```

El servicio fue desarrollado con Flask. Para instalarlo hay que configurar la url de la base de datos (puede ser cualquier base de datos soportada por SQLAlchemy) en el archivo settings.conf (hay un archivo de ejemplo settings.conf.example) o en la variable de entorno DATABASE_URL.
Los datos se generan con el comando
```
galaxyml generate_data 10
```
Y el servicio se levanta con el comando:
```
galaxyml rest_api
```

El servicio escucha en el puerto 5000.

### Desarrollo

#### Diseño

La aplicación se diseñó con un modelo de objetos (enfoque apropiado para un problema de simulación), pero tratando de mantener el código idomático. Como consecuencia, el diseño quizás sea un poco más genérico de lo necesario para resolver el problema concreto.

#### Entorno de desarrollo

Para instalar el ambiente de desarrollo, hay que clonar el repositorio, crear un virtualenv (opcional), activarlo e instalar los requerimientos que se encuentran en el archivo `requirements-dev.txt`.
```
pip install -r requirements-dev.txt
```
Las tareas de soporte de desarrollo se organizaron con Fabric. Las tareas de Fabric se ejecutan con el comando `fab`.

#### Testing

La aplicación se desarrolló usando TDD. Se separaron los tests en tres categorías: unitarios, de aceptación y de integración.

1. Unitarios: Prueban por separado las clases del modelo.
2. Aceptación: Prueban las funciones requeridas desde la perspectiva del usuario.
3. Integración: Prueban la conexión con la base de datos y el servicio REST.

Se usaron las siguientes herramientas para los tests: framework de unit test y de mocking de la librería estándar de Python, py.test para correr los tests (provee una mejor visualización de los resultados que unittest) y Hamcrest para assertions.

Para correr los distintos tests se usan las tareas de Fabric `test.unit`, `test.acceptance` y `test.integration`. También hay una tarea que muestra la información de cobertura y verifica que ésta sea de al menos 95%: `test.coverage`.

#### Análisis de código

La tarea `test.check_convetions` verifica que el código cumpla con las convenciones de codificación PEP8. También se agregó una tarea `test.check_metrics` que provee información de otras métricas usando `pylint` (código documentado, convenciones de nombres, repetición de código, etc.). Se verificó que el código tenga un puntaje por encima de 7.5. Con un poco más de trabajo, esa verificación podría automatizarse.

Para correr todos los tests y los chequeos juntos se puede usar `test.all` o simplemente `test`. 

### Performance

El tiempo necesario para generar el pronóstico de los próximos diez años es despreciable. 
El algoritmo crece linealmente en función de la cantidad de días a pronosticar.
```
In [2]: %timeit galaxyml.summary(10)
1 loops, best of 3: 168 ms per loop
```
Cómo se pued observar la base de la aplicación son las operaciones matemáticas (Python es relativamente lento para este tipo de operaciones).
```
   Ordered by: cumulative time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.003    0.003    0.429    0.429 galaxyml.py:3(<module>)
        1    0.000    0.000    0.268    0.268 galaxyml.py:19(summary)
        1    0.006    0.006    0.266    0.266 forecast.py:36(forecast_for)
     3652    0.011    0.000    0.205    0.000 forecast.py:23(current_weather)
        3    0.001    0.000    0.147    0.049 __init__.py:8(<module>)
        1    0.000    0.000    0.137    0.137 persistence.py:4(<module>)
        2    0.000    0.000    0.104    0.052 __init__.py:9(<module>)
     3611    0.028    0.000    0.102    0.000 planets.py:56(are_planets_aligned)
     8177    0.017    0.000    0.087    0.000 planets.py:70(planets_polygon)
        1    0.001    0.001    0.080    0.080 expression.py:15(<module>)
     3368    0.006    0.000    0.067    0.000 planets.py:78(does_planets_polygon_contain_sun)
    24531    0.013    0.000    0.066    0.000 planets.py:159(cartesian)
    24531    0.033    0.000    0.053    0.000 geometry.py:68(from_r_and_degrees)

```

Si se quisiera prontósticar el clima para un número muy grande de años, una optimización muy sencilla consitiría en verificar los ciclos del sistema solar. Por ejemplo, es fácil comprobar que el sistema solar de la galaxia tiene un ciclo de 360 días. Cada 360 días el planeta Ferengui habrá dado una vuelta alrededor del sol, el planeta Betasoide, 3; y el planeta Vulcano, 5; y todos los planetas habrán vuelto a la posición inicial. Por lo tanto, se podría cambiar el algoritmo para que deje de pronosticar una vez que se cumple el ciclo (verificando que todos los planetas están en posición angular cero), y mejorar los cálculos del resumen y el pronóstico de un día para que tengan en cuenta los ciclos. De esta forma, el tiempo de ejecución sería constante para periodos mayores a 360 días. Esta optimización también ahorraría mucho espacio de almacenamiento al servicio REST. En concreto, para el pronóstico de diez años, se ahorrarían 3290 registros en la base de datos.

### Uso de memoria


![alt text](doc/memory.png "Uso de memoria")
