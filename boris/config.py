"""
This module manages the configuration of the application.
"""
import ConfigParser
import os

SETTINGS_FILE = 'settings.conf'


def get_database_url():
    """
    Returns the database url configuration.
    It tries to get it from a settings.conf file.
    If the file doesn't exist, it looks for it in the DATABASE_URL environment.
    """
    try:
        Config = ConfigParser.ConfigParser()
        Config.read(SETTINGS_FILE)
        return Config.get('Database', 'url')
    except ConfigParser.NoSectionError:
        if 'DATABASE_URL' in os.environ:
            return os.environ['DATABASE_URL']
        raise Exception('Database not configured')
