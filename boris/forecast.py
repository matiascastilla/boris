"""
This module implements the weather forecasting logic.
"""
from datetime import date, datetime
from collections import Counter, namedtuple
from itertools import groupby
from operator import attrgetter, itemgetter
from dateutil.relativedelta import relativedelta


class WeatherForecaster(object):
    """
    A WeatherForecaster generates a forecast for a period of time by simulating
    the succesive daily states of the solar system.
    """

    def __init__(self, solar_system):
        """
        :type solar_system: SolarSystem
        """
        self._solar_system = solar_system

    def current_weather(self):
        """
        Returns weather based on the solar system state
        :rtype: Weather
        """
        if self._solar_system.are_planets_aligned_with_sun():
            return Weather.DROUGHT
        if self._solar_system.are_planets_aligned():
            return Weather.OPTIMAL
        if self._solar_system.does_planets_polygon_contain_sun():
            return Weather.RAINY
        return Weather.REGULAR

    def forecast_for(self, delta, today=None):
        """
        Generates a forecast starting today an spaning for the given
        delta
        :type delta: relativedelta
        :type today: day zero
        :rtype: Forecast
        """
        today = today or date.today()
        period_end = today + delta
        if (isinstance(period_end, datetime) or
                period_end < today + relativedelta(days=1)):
            raise ValueError(
                "Invalid period. "
                "It can't contain hours and it must be greater than one day")
        self._solar_system.reset()
        forecast = Forecast(today)
        for _ in xrange((period_end - today).days):
            # Weird, but seemingly the only way to do it
            weather = self.current_weather()
            forecast.register_next_forecast(
                weather, weather.get_peak_value(self._solar_system))
            self._solar_system.orbit()
        forecast.close()
        return forecast


class classproperty(property):
    """
    Used to decorate class methods so they behave like properties.
    """
    def __get__(self, cls, owner):
        return self.fget.__get__(None, owner)()


class Weather(object):
    """Instances of this class represent possible weather states"""

    _drought = None
    _rainy = None
    _regular = None
    _optimal = None

    # For persistence purposes I need these constants to be created
    # after the classes are augmented. For this reason I came up with
    # this workaround, despite the fact that the methods names violete
    # the coding standards.
    @classproperty
    @classmethod
    def DROUGHT(cls):
        cls._drought = cls._drought or Weather('Drought')
        return cls._drought

    @classproperty
    @classmethod
    def RAINY(cls):
        cls._rainy = cls._rainy or RainyWeather('Rainy')
        return cls._rainy

    @classproperty
    @classmethod
    def REGULAR(cls):
        cls._regular = cls._regular or Weather('Regular')
        return cls._regular

    @classproperty
    @classmethod
    def OPTIMAL(cls):
        cls._optimal = cls._optimal or Weather('Optimal')
        return cls._optimal

    def __init__(self, name):
        """
        :type name: str
        """
        self.name = name

    def get_peak_value(self, solar_system):
        """
        Exctracts from the solar system the value that represent
        the "degree" of the current weather.
        :type solar_system: SolarSystem
        :rtype: float
        """
        return None

    def __repr__(self):
        return '<Weather: {}>'.format(self.name)


class RainyWeather(Weather):
    """Rainy weather knows how to get the rain magnitude"""

    def get_peak_value(self, solar_system):
        return solar_system.planets_perimeter()


def _ensure_closed(func):
    """
    Wrapper to make the method raise an exception if the forecast
    hasn't been closed
    """
    def wrapper(self):
        if not self._closed:
            raise ValueError('Period not closed')
        return func(self)
    return wrapper


class Forecast(object):
    """
    Instances of this class represents forecasts for a given period.
    A forecast is generated through a simulation of the
    succesive states of the solar system.
    """

    def __init__(self, from_date):
        self.from_date = from_date
        self._periods = []
        self._current_period = None
        self._closed = False

    def register_next_forecast(self, weather, value):
        """
        Registers the forecast for the current day
        """
        if (self._current_period is None or
                self._current_period.weather != weather):
            self._close_period()
            self._current_period = Period(self._next_day, weather, value)
        else:
            self._current_period.extend(value)

    def _close_period(self):
        """
        Finishes current period
        """
        if self._current_period:
            self._periods.append(self._current_period)

    def close(self):
        """
        Closes the forecast
        """
        if self._current_period is None:
            raise ValueError('No forecasts registered')
        self._close_period()
        self._closed = True

    def summary(self):
        """
        Returns a summary of the weather periods
        :rtype: dict
        """
        return {
            'period_count': dict(
                Counter(period.weather for period in self.periods)),
            'peaks': {
                weather: max(
                    ((p.peak.day, round(p.peak.value, 2)) for p in periods),
                    key=itemgetter(1))
                for weather, periods in
                groupby((p for p in self.periods if p.peak is not None),
                        attrgetter('weather'))
            }
        }

    @property
    @_ensure_closed
    def periods(self):
        """
        Returns the forecasted periods
        """
        return self._periods

    @property
    @_ensure_closed
    def end_day(self):
        """
        Return the end day
        """
        return self._periods[-1].end_day

    @property
    def _next_day(self):
        """
        Establishes the next day
        """
        return (self._current_period.end_day + 1
                if self._current_period else 0)

Peak = namedtuple('Peak', ('day', 'value'))


class Period(object):
    """
    A weather forecasted for a period of days
    """
    def __init__(self, start_day, weather, value):
        """
        :type start_day: int
        :type end_day: int
        :type weather: Weather
        :type value: float
        """
        self.start_day = start_day
        self.end_day = start_day
        self.weather = weather
        self.peak = None
        self._track_peak(value)

    def extend(self, value):
        """
        Extends the period by one day
        """
        self.end_day += 1
        self._track_peak(value)

    def _track_peak(self, value):
        """
        Checks wheather the current weather *degree* is the maximum
        and registers it if so
        :type value: float
        """
        if value is None:
            return
        if self.peak is None or self.peak.value < value:
            self.peak = Peak(self.end_day, value)

    def __repr__(self):
        desc = 'Period: {} from day {} to day {}'.format(
            self.weather.name, self.start_day, self.end_day)
        if self.peak:
            desc += ' with a peak of {} on day {}'.format(
                self.peak.value, self.peak.day)
        return desc
