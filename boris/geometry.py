"""
This module provides support for some geometry operations.
"""
from math import cos, sin, atan, sqrt


class Polygon(object):
    """
    Provides basic operation on polygons
    """

    def __init__(self, vertices):
        """
        :type verices: list or tuple
        """
        if len(vertices) < 3:
            raise ValueError('Polygon must have at least three vertices')
        self.vertices = vertices

    def segments(self):
        """
        Returns an iterator through the polygon segments
        :rtype: iterator
        """
        for i in xrange(len(self.vertices) - 1):
            yield self.vertices[i], self.vertices[i + 1]
        yield self.vertices[-1], self.vertices[0]

    def contains_point(self, point):
        """
        Algorithm ported from Pharo Smalltalk
        :type point: Point
        :rtype: boolean
        """
        wind = 0
        for p1, p2 in self.segments():
            if p1.y <= point.y:
                if p2.y == point.y:
                    if point.direction_to_line(p1, p2) == 0:
                        return True
                else:
                    if (p2.y > point.y and
                            (point.direction_to_line(p1, p2) > 0)):
                        wind = wind + 1
            else:
                if p2.y == point.y:
                    if point.direction_to_line(p1, p2) == 0:
                        return True
                if p2.y < point.y and point.direction_to_line(p1, p2) < 0:
                    wind = wind - 1
        return wind != 0

    def perimeter(self):
        """
        Returns the perimeter of the polygon
        :rtype: float
        """
        return sum(p1.distance_to(p2) for p1, p2 in self.segments())


class Point(object):
    """
    Provides basic operation on points
    """

    RADIANS_PER_DEGREE = 0.017453292519943295

    @classmethod
    def from_r_and_degrees(cls, r, degrees):
        radians = degrees * cls.RADIANS_PER_DEGREE
        return Point(r * cos(radians), r * sin(radians))

    @classmethod
    def origin(cls):
        return Point(0, 0)

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def bearing_to(self, another_point):
        """
        Returns the angle to another point. Ported from Pharo Smalltalk
        :type point: Point
        :rtype: int
        """
        delta_x = another_point.x - self.x
        delta_y = another_point.y - self.y
        if abs(delta_x) < 0.001:
            if delta_y > 0:
                return 180
            else:
                return 0
        tmp = 90 if delta_x >= 0 else 270
        return round(
            tmp -
            ((atan(delta_y / delta_x) * - 1) /
             self.RADIANS_PER_DEGREE))

    def direction_to_line(self, from_point, to_point):
        return (((to_point.x - from_point.x) * (self.y - from_point.y)) -
                ((self.x - from_point.x) * (to_point.y - from_point.y)))

    def distance_to(self, other):
        """
        Returns the distance to another point
        :type other: Point
        :rtype: float
        """
        return sqrt((other.x - self.x)**2 + (other.y - self.y)**2)

    def __eq__(self, other):
        if not isinstance(other, Point):
            return False
        return (self.x, self.y) == (other.x, other.y)

    def __neq__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return '<Point: {}, {}>'.format(self.x, self.y)
