# encoding: utf-8
"""
This modules handles the peristent storage of the forecasts
"""
from sqlalchemy import Table, MetaData, \
    Column, Integer, String, Boolean, ForeignKey, create_engine
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm import mapper, sessionmaker, relationship,\
    polymorphic_union
from boris.forecast import Weather, RainyWeather, Period, Forecast


def _create_metadata():
    """
    Creates the database and the orm mappings
    :rtype: Metadata
    """

    metadata = MetaData()

    forecast_table = Table(
        'forecast', metadata,
        Column('id', Integer, primary_key=True),
        Column('_closed', Boolean))

    weather_table = Table(
        'weather', metadata,
        Column('id', Integer, primary_key=True),
        Column('name', String(50)))

    rainy_table = Table(
        'rainy_weather', metadata,
        Column('id', Integer, primary_key=True),
        Column('name', String(50)))

    period_table = Table(
        'periods', metadata,
        Column('id', Integer, primary_key=True),
        Column('start_day', Integer, index=True),
        Column('end_day', Integer, index=True),
        Column('weather_id', ForeignKey(weather_table.c.id)),
        Column('forecast_id', ForeignKey(forecast_table.c.id)))

    pjoin = polymorphic_union({
        'generic': weather_table,
        'rainy': rainy_table
    }, 'type', 'pjoin')

    weather_mapper = mapper(
        Weather, weather_table,
        with_polymorphic=('*', pjoin),
        polymorphic_on=pjoin.c.type,
        polymorphic_identity='generic',
        concrete=True)
    mapper(RainyWeather,
           weather_table,
           inherits=weather_mapper,
           polymorphic_identity='rainy',
           concrete=True)
    mapper(Period, period_table, properties={
        'weather': relationship(Weather)})
    mapper(Forecast, forecast_table, properties={
        '_periods': relationship(Period, lazy='dynamic')})

    return metadata


class ForecastRepository(object):
    """
    Stores and retrieves forecast data
    """

    _metadata = None

    def __init__(self, database_url):
        """
        :type database_url: str
        """
        if self._metadata is None:
            self.__class__._metadata = _create_metadata()
        self._session_class = sessionmaker()
        self._engine = create_engine(database_url)
        self._session_class.configure(bind=self._engine)
        self._current_session = None

    def _session(self):
        self._current_session = self._current_session or self._session_class()
        return self._current_session

    def save(self, forecast):
        """
        Saves the forecast
        :type forecast: Forecast
        """
        self._session().add(forecast)
        self._commit()

    def weather_for_day(self, day):
        """
        Returns the weather forecasted for a given day
        :type day: int
        :rtype: str
        """
        forecast = self._session().query(Forecast)[-1]
        res = None
        try:
            period = forecast.periods.filter(
                (Period.start_day <= day) &
                (Period.end_day >= day)).one()
            res = period.weather.name
            self._commit()
            return res
        except NoResultFound:
            self._commit()
            raise ValueError('No data available for day {}'.format(day))

    def _commit(self):
        self._session().commit()
        self._session().close()
        self._current_session = None

    def create_db(self):
        self._metadata.create_all(self._engine)
