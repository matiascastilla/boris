"""
This module provides a model for the simulation of the solar system
"""
from boris.geometry import Polygon, Point


class SolarSystem(object):
    """
    Represents the solar system
    """

    # I accept some degree of error to deal with the fact that
    # I move the planets a whole day at a time and I don't want to miss
    # any alignment.
    MAX_ALIGN_ERROR = 2

    # I need better polygon algithms to make de solution generic
    MAX_PLANETS = 3

    def __init__(self):
        self.planets = []

    def add_planet(self, name, distance, direction, velocity):
        """
        Adds a planet to the solar system
        :type name: str
        :type distance: int: Distance in km
        :type direction: Direction: Orbit direction
        :type velocity: Angular velocity in degrees per day
        :rtype: SolarSystem: for fluent interface
        """
        if len(self.planets) == self.MAX_PLANETS:
            raise ValueError('Max number of planets is {}'
                             .format(self.MAX_PLANETS))
        self.planets.append(Planet(name, distance, direction, velocity))
        return self

    def orbit(self):
        """
        Makes the system orbit one day
        """
        for planet in self.planets:
            planet.orbit()

    def reset(self):
        """
        Resets the system to the original position
        (all planets located at an angle of zero).
        """
        for planet in self.planets:
            planet.reset_angle()

    def are_planets_aligned_with_sun(self):
        """
        Answers wheather all the planets are aligned with the sun
        :rtype: bool
        """
        if not self.planets:
            raise ValueError('Cannot determine alignment')
        angles = [planet.location.angle % 180 for planet in self.planets]
        avg = sum(angles) / len(angles)
        return all(abs(angle - avg) < self.MAX_ALIGN_ERROR
                   for angle in angles)

    def are_planets_aligned(self):
        """
        Answers weather all the planets are aligned
        """
        if not self.planets:
            raise ValueError('Cannot determine alignment')
        bearings = [
            p1.bearing_to(p2) % 180 for p1, p2
            in list(self.planets_polygon().segments())[:-1]
        ]
        avg = sum(bearings) / len(bearings)
        return all(abs(bearing - avg) < self.MAX_ALIGN_ERROR
                   for bearing in bearings)

    def planets_polygon(self):
        """
        Returns the polygon formed by the planets
        :rtype: Polygon
        """
        return Polygon([planet.location.cartesian()
                        for planet in self.planets])

    def does_planets_polygon_contain_sun(self):
        """
        Answer wheather the polygon formed by the planets contains the sun
        (located at the origin)
        :rtype: bool
        """
        return self.planets_polygon().contains_point(Point.origin())

    def planets_perimeter(self):
        """
        Returns the perimeter of the polygon formed by the planets
        :rtype: float
        """
        return self.planets_polygon().perimeter()


class Planet(object):
    """
    A planet in the solar system
    """

    def __init__(self, name, distance, direction, velocity):
        """
        :type name: str
        :type distance: int: Distance in km
        :type direction: Direction: Orbit direction
        :type velocity: Angular velocity in degrees per day
        """
        self.name = name
        self.direction = direction
        self.location = Location(distance)
        self.velocity = velocity

    def orbit(self):
        """
        Makes the planet orbit one day
        """
        self.location.rotate(self.velocity * self.direction.factor)

    def reset_angle(self):
        """
        Reset planet position to angle=0
        """
        self.location.reset()

    @property
    def distance(self):
        """
        Returns the distance to the sun in km
        :rtype: int
        """
        return self.location.distance


class Location(object):
    """
    A location holds the current location of a planet expressed in
    terms of a distance to the origin and an angle in degrees.
    """

    def __init__(self, distance):
        """
        Initializes the location to angle zero and a given distance
        :type distance: int: Distance in km
        """
        self.distance = distance
        self.angle = 0

    def rotate(self, degrees):
        """
        Rotates the location by an angle of <degrees>
        :type degrees: int
        """
        self.angle = (self.angle + degrees) % 360

    def reset(self):
        """
        Resets the angle to zero
        """
        self.angle = 0

    def cartesian(self):
        """
        Returns the location translated to cartesian coordinates
        :rtype: Point
        """
        return Point.from_r_and_degrees(self.distance, self.angle)


class _Direction(object):
    """
    Represents the posible movement directions of the planets.
    It's just a device to avoid the direct use of integers
    """

    def __init__(self, factor):
        """
        :type factor: int: Should be 1 or -1
        """
        self.factor = factor


CLOCKWISE = _Direction(1)
ANTICLOCKWISE = _Direction(-1)
