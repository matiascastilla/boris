from fabric.operations import local
from fabric.api import task
import pytest

_COVERAGE_THRESHOLD = 95


@task
def integration():
    """Runs integrations tests"""
    pytest.main(['tests/integration'])


@task
def unit():
    """Runs unit tests"""
    pytest.main(['tests/unit'])


@task
def acceptance():
    """Runs acceptance tests"""
    pytest.main(['tests/acceptance'])


@task
def check_conventions():
    """Checks code for pep8 compilance"""
    local('flake8 boris tests')


@task
def check_metrics():
    local('pylint boris mercadolibre')


@task(default=True)
def all():
    """Runs all level tests and code analysis"""
    pytest.main(['tests/unit', 'tests/acceptance', 'tests/integration'])
    check_conventions()
    coverage()


@task
def coverage():
    local('python -m coverage run -m pytest '
          'tests/unit tests/acceptance tests/integration  -q;'
          'python -m coverage report --omit=.env/*,tests/* --fail-under={}'
          .format(_COVERAGE_THRESHOLD))
