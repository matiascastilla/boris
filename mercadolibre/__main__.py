# encoding: utf-8

import sys
from dateutil.relativedelta import relativedelta
from boris.forecast import WeatherForecaster
from boris.planets import SolarSystem, CLOCKWISE, ANTICLOCKWISE
from boris import config
from boris.persistence import ForecastRepository
from prettytable import PrettyTable


_solar_system = SolarSystem()
(_solar_system
    .add_planet('Ferengi', 500, CLOCKWISE, 1)
    .add_planet('Betasoide', 2000, CLOCKWISE, 3)
    .add_planet('Vulcano', 1000, ANTICLOCKWISE, 5))


def summary(years):
    """
    Prints the weather summary for a given period
    :type years: int
    """
    forecaster = WeatherForecaster(_solar_system)
    summary = forecaster.forecast_for(
        relativedelta(years=years)).summary()
    period_table = PrettyTable(['Weather', 'Number of periods'])
    for weather, count in summary['period_count'].iteritems():
        period_table.add_row([weather.name, count])
    peaks_table = PrettyTable(['Weather', 'Peak day', 'Peak value'])
    for weather, (peak_date, peak_value) in summary['peaks'].iteritems():
        peaks_table.add_row([weather.name, peak_date, peak_value])
    print 'Periods:'
    print period_table
    print
    print 'Peaks:'
    print peaks_table


def generate_data(years):
    """
    Generates the weather database for a given period
    :type years: int
    """
    forecaster = WeatherForecaster(_solar_system)
    repository = ForecastRepository(config.get_database_url())
    repository.create_db()
    repository.save(forecaster.forecast_for(
        relativedelta(years=years)))


def rest_api():
    import rest_api
    rest_api.app.run()


def print_usage():
    """
    Prints command usage instructions
    """
    print 'Usage: galaxyml <command> YEARS'
    print 'Commands:'
    print '    summary        Prints the forecast summary'
    print '    generate_data  Generates the database with forecast data'
    print '    rest_api       Runs the rest service'


def main():
    if len(sys.argv) < 2:
        print_usage()
        sys.exit(1)
    command = sys.argv[1]
    if command in ('summary', 'generate_data'):
        if len(sys.argv) != 3:
            print_usage()
            sys.exit(1)
        try:
            years = int(sys.argv[2])
        except ValueError:
            print_usage()
            sys.exit(2)
        if command == 'summary':
            summary(years)
        elif command == 'generate_data':
            generate_data(years)
    elif command == 'rest_api':
        rest_api()
    elif command == 'help':
        print_usage()
    else:
        print 'Unkown command: {}'.format(command)
        print "Use 'galaxyml help' for help"

if __name__ == '__main__':
    main()
