# encoding: utf-8

import logging
from flask import Flask, request, jsonify
from boris.persistence import ForecastRepository
from boris import config

logging.getLogger().addHandler(logging.StreamHandler())

app = Flask(__name__)

TRANSLATION = {
    'Regular': u'Normal',
    'Rainy': u'Lluvia',
    'Optimal': u'Condiciones optimas',
    'Drought': u'Sequía'
}

db_url = config.get_database_url()


@app.route("/clima", methods=['GET'])
def weather():
    try:
        day = request.args.get('dia', '')
        repository = ForecastRepository(db_url)
        try:
            weather = repository.weather_for_day(int(day))
        except ValueError:
            weather = 'Sin datos'
        return jsonify(**{
            'dia': day, 'clima': _translate(weather)
            })
    except Exception as e:
        logging.exception(e)
        raise


def _translate(weather):
    if weather not in TRANSLATION:
        raise ValueError('Unkown weather')
    return TRANSLATION[weather].encode('utf8', 'ignore')


if __name__ == '__main__':
    app.run(debug=False)
