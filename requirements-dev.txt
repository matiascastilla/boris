-r requirements.txt
fabric==1.11.1
pytest==2.9.2
pyhamcrest==1.9.0
mock==2.0.0
coverage==4.1
flake8==2.5.4
pylint==1.5.6
