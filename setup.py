# encoding: utf-8

from setuptools import setup
from pip.req import parse_requirements


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...


install_reqs = parse_requirements('requirements-base.txt', session=False)
reqs = [str(ir.req) for ir in install_reqs]

setup(
    name="boris",
    version="0.0.1",
    author="Matías Castilla",
    author_email="matiascastilla@gmail.com",
    description=("Test Mercado Libre"),
    keywords="forecast",
    url="https://bitbucket.org/matiascastilla/boris",
    install_requires=reqs,
    packages=['boris', 'mercadolibre'],
    entry_points={
        'console_scripts': ['galaxyml = mercadolibre.__main__:main'],
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
