# encoding: utf-8

from unittest import TestCase
from dateutil.relativedelta import relativedelta
from boris.forecast import WeatherForecaster, Peak, Weather
from boris.planets import SolarSystem, CLOCKWISE, ANTICLOCKWISE
from hamcrest import assert_that, has_length, has_properties


class TestForecast(TestCase):

    def setUp(self):
        solar_system = SolarSystem()
        (solar_system
            .add_planet('Ferengi', 500, CLOCKWISE, 1)
            .add_planet('Betasoide', 2000, CLOCKWISE, 3)
            .add_planet('Vulcano', 1000, ANTICLOCKWISE, 5))
        self.forecaster = WeatherForecaster(solar_system)

    def test_it_generates_forecast_for_a_period_of_time(self):
        forecast = self.forecaster.forecast_for(relativedelta(days=30))
        assert_that(forecast.periods, has_length(5))
        assert_that(forecast.periods[0], has_properties({
            'weather': Weather.DROUGHT, 'start_day': 0,
            'end_day': 0, 'peak': None
        }))
        assert_that(forecast.periods[1], has_properties({
            'weather': Weather.REGULAR, 'start_day': 1,
            'end_day': 17, 'peak': None
        }))
        assert_that(forecast.periods[2], has_properties({
            'weather': Weather.OPTIMAL, 'start_day': 18,
            'end_day': 19, 'peak': None
        }))
        assert_that(forecast.periods[3], has_properties({
            'weather': Weather.REGULAR, 'start_day': 20,
            'end_day': 22, 'peak': None
        }))
        assert_that(forecast.periods[4], has_properties({
            'weather': Weather.RAINY, 'start_day': 23,
            'end_day': 29,
            'peak': Peak(25, 6135.928957012673)}))
