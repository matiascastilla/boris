from unittest import TestCase
from dateutil.relativedelta import relativedelta
from boris.forecast import WeatherForecaster, Weather
from boris.planets import SolarSystem, CLOCKWISE, ANTICLOCKWISE
from hamcrest import assert_that, equal_to


class TestForeCast(TestCase):

    def setUp(self):
        solar_system = SolarSystem()
        (solar_system
            .add_planet('Ferengi', 500, CLOCKWISE, 1)
            .add_planet('Betasoide', 2000, CLOCKWISE, 3)
            .add_planet('Vulcano', 1000, ANTICLOCKWISE, 5))
        self.forecaster = WeatherForecaster(solar_system)

    def test_it_generates_a_weather_summary_for_a_period_of_time(self):
        forecast = self.forecaster.forecast_for(relativedelta(days=30))
        assert_that(forecast.summary(), equal_to({
            'period_count': {
                Weather.DROUGHT: 1,
                Weather.RAINY: 1,
                Weather.REGULAR: 2,
                Weather.OPTIMAL: 1
            },
            'peaks': {
                Weather.RAINY: (25, 6135.93)
            }
        }))
