import os
from unittest import TestCase
from hamcrest import assert_that, equal_to, calling, raises
from dateutil.relativedelta import relativedelta
from boris.planets import SolarSystem, CLOCKWISE, ANTICLOCKWISE
from boris.forecast import WeatherForecaster, Weather
from boris.persistence import ForecastRepository


class PeristForecastTest(TestCase):

    DB_PATH = '/tmp/boris.sqlite'
    DB_URL = 'sqlite:///' + DB_PATH

    @classmethod
    def setUpClass(cls):
        if os.path.exists(cls.DB_PATH):
            os.remove(cls.DB_PATH)
        # I need to do this trick to force the constants to be
        # created again with persistence metadata.
        Weather._rainy = Weather._regular = \
            Weather._optimal = Weather._drought = None

    def setUp(self):
        solar_system = SolarSystem()
        (solar_system
            .add_planet('Ferengi', 500, CLOCKWISE, 1)
            .add_planet('Betasoide', 2000, CLOCKWISE, 3)
            .add_planet('Vulcano', 1000, ANTICLOCKWISE, 5))
        forecaster = WeatherForecaster(solar_system)
        self.repository = ForecastRepository(self.DB_URL)
        self.repository.create_db()
        self.repository.save(
            forecaster.forecast_for(relativedelta(days=30)))

    def test_perists_forecast(self):
        assert_that(self.repository.weather_for_day(1), equal_to('Regular'))
        assert_that(self.repository.weather_for_day(25), equal_to('Rainy'))

    def test_it_fails_if_there_is_no_available_data(self):
        assert_that(calling(self.repository.weather_for_day).with_args(100),
                    raises(ValueError, 'No data available for day 100'))
