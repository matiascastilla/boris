# encoding: utf-8

from unittest import TestCase
from hamcrest import assert_that, equal_to, has_properties, has_length, \
    calling, raises
from boris.forecast import Forecast, Peak, Weather


class ForecastTest(TestCase):

    def setUp(self):
        self.forecast = Forecast(0)
        self.forecast.register_next_forecast(Weather.RAINY, 100)
        self.forecast.register_next_forecast(Weather.RAINY, 200)
        self.forecast.register_next_forecast(Weather.DROUGHT, None)
        self.forecast.close()

    def test_it_is_initialized_with_the_start_day(self):
        forecast = Forecast(1)
        assert_that(forecast.from_date, equal_to(1))

    def test_it_registers_daily_forecasts_and_group_them_in_periods(self):
        assert_that(self.forecast.periods, has_length(2))
        assert_that(self.forecast.periods[0], has_properties({
            'start_day': 0, 'end_day': 1,
            'weather': Weather.RAINY
        }))
        assert_that(self.forecast.periods[1], has_properties({
            'start_day': 2, 'end_day': 2,
            'weather': Weather.DROUGHT
        }))

    def test_it_tracks_the_peak_value_of_the_period(self):
        assert_that(self.forecast.periods[0].peak,
                    equal_to(Peak(1, 200)))

    def test_end_day_is_the_last_period_end_day(self):
        assert_that(self.forecast.end_day,
                    equal_to(self.forecast.periods[-1].end_day))

    def test_it_fails_to_return_periods_or_end_day_if_not_closed(self):
        forecast = Forecast(0)
        forecast.register_next_forecast(Weather.RAINY, 100)
        assert_that(calling(lambda: forecast.periods),
                    raises(ValueError, 'Period not closed'))
        assert_that(calling(lambda: forecast.end_day),
                    raises(ValueError, 'Period not closed'))

    def test_it_fails_to_close_if_no_forecast_has_been_added(self):
        forecast = Forecast(0)
        assert_that(calling(forecast.close),
                    raises(ValueError, 'No forecasts registered'))

    def test_it_sumimarizes_itself(self):
        assert_that(self.forecast.summary(), equal_to({
            'period_count': {
                Weather.RAINY: 1,
                Weather.DROUGHT: 1
            },
            'peaks': {
                Weather.RAINY: (1, 200)
            }
        }))
