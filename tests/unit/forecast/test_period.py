# encoding: utf-8

from unittest import TestCase
from boris.forecast import Period, Peak, Weather
from hamcrest import assert_that, has_properties, equal_to, is_


class PeriodTest(TestCase):

    def setUp(self):
        self.period = Period(0, Weather.RAINY, 100)

    def test_it_is_initialized_with_weather__start_date_and_a_value(self):
        period = Period(0, Weather.RAINY, 100)
        assert_that(period, has_properties(
            {'weather': Weather.RAINY, 'start_day': 0,
             'end_day': 0}))

    def test_it_can_be_extended_by_a_day(self):
        self.period.extend(None)
        assert_that(self.period.end_day, equal_to(1))

    def test_it_tracks_the_peak_value(self):
        self.period.extend(50)
        assert_that(self.period.peak, equal_to(Peak(0, 100)))
        self.period.extend(200)
        assert_that(self.period.peak, equal_to(Peak(2, 200)))

    def test_it_ignores_none_values(self):
        self.period.extend(None)
        assert_that(self.period.peak.day, equal_to(0))
        another_period = Period(0, Weather.RAINY, None)
        assert_that(another_period.peak, is_(None))
        another_period.extend(10)
        assert_that(another_period.peak, equal_to(Peak(1, 10)))
        another_period.extend(None)
        assert_that(another_period.peak, equal_to(Peak(1, 10)))
