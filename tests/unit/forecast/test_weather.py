from unittest import TestCase
from hamcrest import assert_that, equal_to
from boris.forecast import Weather


class WeatherTest(TestCase):

    def test_it_is_initialized_with_a_name(self):
        weather = Weather('Rainy')
        assert_that(weather.name, equal_to('Rainy'))
