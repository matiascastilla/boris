from unittest import TestCase
from boris.forecast import WeatherForecaster, Peak, Weather
from boris.planets import SolarSystem
from mock import Mock
from hamcrest import assert_that, is_, has_length, has_properties, calling, \
    raises
from dateutil.relativedelta import relativedelta


class WeatherForecasterTest(TestCase):

    def setUp(self):
        self.solar_system = SolarSystem()
        self.forecaster = WeatherForecaster(self.solar_system)

    def test_it_forecasts_drought_if_planets_are_aligned_with_sun(self):
        self._mock_solar_system_state(aligned_with_sun=True)
        assert_that(self.forecaster.current_weather(), is_(Weather.DROUGHT))

    def test_it_forecasts_optimal_conditions_if_planets_are_aligned(self):
        self._mock_solar_system_state(aligned=True)
        assert_that(self.forecaster.current_weather(), is_(Weather.OPTIMAL))

    def test_it_forecasts_rain_if_planets_polygon_contains_sun(self):
        self._mock_solar_system_state(contains_sun=True)
        assert_that(self.forecaster.current_weather(), is_(Weather.RAINY))

    def test_it_forecasts_regular_wetaher_otherwise(self):
        self._mock_solar_system_state(
            aligned_with_sun=False, aligned=False,  contains_sun=False)
        assert_that(self.forecaster.current_weather(), is_(Weather.REGULAR))

    def test_it_generates_a_forecast_for_a_given_time_span(self):
        self.forecaster.current_weather = Mock()
        self.forecaster.current_weather.side_effect = [
            Weather.DROUGHT, Weather.RAINY, Weather.RAINY]
        self.solar_system.planets_perimeter = Mock()
        self.solar_system.planets_perimeter.side_effect = [100, 200]
        forecast = self.forecaster.forecast_for(relativedelta(days=3))
        assert_that(forecast.periods, has_length(2))
        assert_that(forecast.periods[0], has_properties({
            'weather': Weather.DROUGHT, 'start_day': 0,
            'end_day': 0, 'peak': None
        }))
        assert_that(forecast.periods[1], has_properties({
            'weather': Weather.RAINY, 'start_day': 1,
            'end_day': 2, 'peak': Peak(2, 200)
        }))

    def test_it_fails_if_period_is_less_than_a_day(self):
        assert_that(calling(self.forecaster.forecast_for)
                    .with_args(relativedelta(hours=23)),
                    raises(ValueError, 'Invalid period.*'))

    def _mock_solar_system_state(self, aligned_with_sun=False, aligned=False,
                                 contains_sun=False):
        self.solar_system.are_planets_aligned_with_sun = \
            Mock(return_value=aligned_with_sun)
        self.solar_system.are_planets_aligned = \
            Mock(return_value=aligned)
        self.solar_system.does_planets_polygon_contain_sun = \
            Mock(return_value=contains_sun)
