from unittest import TestCase
from boris.geometry import Point
from hamcrest import assert_that, equal_to, is_not


class PointTest(TestCase):

    def test_it_can_be_created_from_r_and_angle_in_degrees(self):
        point = Point.from_r_and_degrees(1000, 3)
        assert_that(point.x, equal_to(998.6295347545738))
        assert_that(point.y, equal_to(52.33595624294384))

    def test_it_returns_the_bearing_to_another_point(self):
        assert_that(Point(0, 0).bearing_to(Point(10, 10)),
                    equal_to(135))
        assert_that(Point(0, 0).bearing_to(Point(10, 0)),
                    equal_to(90))

    def test_equals_if_coordinates_are_equal(self):
        assert_that(Point(10, 10), equal_to(Point(10, 10)))
        assert_that(Point(5, 5), is_not(equal_to(Point(5, 7))))
        assert_that(Point(5, 5), is_not(equal_to('5, 5')))

    def test_origin(self):
        assert_that(Point.origin(), equal_to(Point(0, 0)))

    def test_it_determines_the_distance_to_another_point(self):
        assert_that(Point(10, 10).distance_to(Point(10, 20)), equal_to(10))
        assert_that(round(Point(5, 5).distance_to(Point(10, 10)), 2),
                    equal_to(7.07))
        assert_that(round(Point(-5, -5).distance_to(Point(-10, -10)), 2),
                    equal_to(7.07))
