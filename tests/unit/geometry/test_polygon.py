from unittest import TestCase
from boris.geometry import Polygon, Point
from hamcrest import assert_that, contains, is_, calling, raises, equal_to


class PolygonTest(TestCase):

    def setUp(self):
        self.polygon = Polygon([Point(10, 10), Point(20, 20), Point(30, 10)])

    def test_it_is_initialized_with_a_sequence_of_vertices(self):
        polygon = Polygon([Point(10, 10), Point(20, 20), Point(30, 10)])
        assert_that(polygon.vertices,
                    contains(Point(10, 10), Point(20, 20), Point(30, 10)))

    def test_it_must_have_at_least_three_vertices(self):
        assert_that(calling(Polygon).with_args([Point(10, 10), Point(20, 20)]),
                    raises(ValueError,
                           'Polygon must have at least three vertices'))

    def test_it_returns_a_an_interetor_trought_its_segments(self):
        assert_that(list(self.polygon.segments()), contains(
            (Point(10, 10), Point(20, 20)), (Point(20, 20), Point(30, 10)),
            (Point(30, 10), Point(10, 10))))

    def test_it_can_say_whether_it_contains_a_point(self):
        assert_that(self.polygon.contains_point(Point(15, 15)), is_(True))
        assert_that(self.polygon.contains_point(Point(0, 0)), is_(False))

    def test_but_contains_point(self):
        polygon = Polygon([
            Point(499.923847578, 8.72620321864),
            Point(1997.25906951, 104.671912486),
            Point(996.194698092, -87.1557427477)])
        assert_that(polygon.contains_point(Point.origin()), is_(False))

    def test_it_knows_its_perimeter(self):
        assert_that(round(self.polygon.perimeter(), 2), equal_to(48.28))
