from unittest import TestCase
from boris.planets import Location
from boris.geometry import Point
from hamcrest import assert_that, equal_to, instance_of, has_properties


class LocationTest(TestCase):

    def setUp(self):
        self.location = Location(1000)

    def test_it_is_initialized_with_the_distance(self):
        location = Location(1000)
        assert_that(location.distance, equal_to(1000))

    def test_angle_is_initialized_to_zero(self):
        assert_that(self.location.angle, equal_to(0))

    def test_it_can_be_rotated_a_number_of_degrees(self):
        self.location.rotate(3)
        assert_that(self.location.angle, equal_to(3))
        self.location.rotate(363)
        assert_that(self.location.angle, equal_to(6))

    def test_it_can_be_rotated_with_a_negative_number_of_degrees(self):
        self.location.rotate(-3)
        assert_that(self.location.angle, equal_to(357))
        self.location.rotate(-3)
        assert_that(self.location.angle, equal_to(354))

    def test_it_can_be_reset(self):
        self.location.rotate(-3)
        self.location.reset()
        assert_that(self.location.angle, equal_to(0))

    def test_returns_the_cartesian_coordinates(self):
        self.location.rotate(3)
        assert_that(self.location.cartesian(), instance_of(Point))
        assert_that(self.location.cartesian(), has_properties({
            'x': 998.6295347545738, 'y': 52.33595624294384
        }))
