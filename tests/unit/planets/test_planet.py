from unittest import TestCase
from boris.planets import Planet, ANTICLOCKWISE, CLOCKWISE
from hamcrest import assert_that, has_properties, equal_to


class PlanetTest(TestCase):

    def setUp(self):
        self.planet = Planet(
            'Betelgeuse Five', distance=1000, velocity=3,
            direction=ANTICLOCKWISE)
        self.another_planet = Planet(
            'Another planet', distance=500, velocity=2,
            direction=CLOCKWISE)

    def test_initialization(self):
        planet = Planet(
            'Betelgeuse Five', distance=1000, velocity=3,
            direction=CLOCKWISE)
        assert_that(planet, has_properties({
            'name': 'Betelgeuse Five',
            'distance': 1000,
            'velocity': 3,
            'direction': CLOCKWISE
        }))

    def test_it_orbits_daily_according_to_its_velocity_and_direction(self):
        self.planet.orbit()
        assert_that(self.planet.location.angle, equal_to(357))
        self.planet.orbit()
        assert_that(self.planet.location.angle, equal_to(354))
        self.another_planet.orbit()
        assert_that(self.another_planet.location.angle, equal_to(2))

    def test_it_can_reset_angular_location_to_zero(self):
        self.planet.orbit()
        self.planet.reset_angle()
        assert_that(self.planet.location.angle, equal_to(0))
