from unittest import TestCase
from boris.planets import SolarSystem, CLOCKWISE, ANTICLOCKWISE
from boris.geometry import Polygon
from hamcrest import assert_that, has_length, has_properties, is_, \
    equal_to, instance_of, contains, calling, raises
from mock import Mock


class SolarSystemTest(TestCase):

    def setUp(self):
        self.solar_system = SolarSystem()
        self.solar_system.add_planet(
            'Betelgeuse Five', distance=500, velocity=3,
            direction=CLOCKWISE)
        self.solar_system.add_planet(
            'Aldebaran', distance=2000, velocity=1,
            direction=ANTICLOCKWISE)
        self.betelgeuse = self.solar_system.planets[0]
        self.aldebaran = self.solar_system.planets[1]

    def test_it_has_a_collection_of_planets(self):
        res = self.solar_system.add_planet(
            'Another planet', distance=1000, velocity=3,
            direction=CLOCKWISE)
        assert_that(self.solar_system.planets, has_length(3), 'Planet added')
        assert_that(self.solar_system.planets[-1], has_properties({
            'name': 'Another planet',
            'distance': 1000,
            'velocity': 3,
            'direction': CLOCKWISE
        }), 'New planet properties')
        assert_that(res, is_(self.solar_system), 'Fluent interface')

    def test_it_can_be_reset_to_original_planets_positions(self):
        self.solar_system.orbit()
        self.solar_system.reset()
        assert_that(self.solar_system.planets[0].location.angle, equal_to(0))

    def test_it_makes_the_planets_orbit(self):
        self.solar_system.orbit()
        assert_that(self.betelgeuse.location.angle, equal_to(3))
        assert_that(self.aldebaran.location.angle, equal_to(359))

    def test_it_knows_whether_the_planets_are_algined_with_sun(self):
        assert_that(self.solar_system.are_planets_aligned_with_sun(),
                    is_(True), 'Aligned at initial position')
        self._set_angles(10, 10)
        assert_that(self.solar_system.are_planets_aligned_with_sun(),
                    is_(True), 'Same angle')
        self.aldebaran.location.angle = 190
        assert_that(self.solar_system.are_planets_aligned_with_sun(),
                    is_(True), 'Oposite angle')
        self.aldebaran.location.angle = 100
        assert_that(self.solar_system.are_planets_aligned_with_sun(),
                    is_(False), 'Not aligned')

    def test_it_knows_whether_the_planets_are_aligned(self):
        self.solar_system.add_planet('Other', distance=1000, velocity=5,
                                     direction=CLOCKWISE)
        self._set_angles(47, 141, 125)
        assert_that(self.solar_system.are_planets_aligned(), is_(True))
        self._set_angles(64, 192, 40)
        assert_that(self.solar_system.are_planets_aligned(), is_(True))
        self._set_angles(116, 348, 140)
        assert_that(self.solar_system.are_planets_aligned(), is_(True))
        self._set_angles(100, 120, 30)
        assert_that(self.solar_system.are_planets_aligned(), is_(False))

    def test_it_fails_to_determine_aligment_if_it_has_no_planets(self):
        assert_that(calling(SolarSystem().are_planets_aligned_with_sun),
                    raises(ValueError, 'Cannot determine alignment'))
        assert_that(calling(SolarSystem().are_planets_aligned),
                    raises(ValueError, 'Cannot determine alignment'))

    def test_it_returns_the_planet_polygon(self):
        self.solar_system.add_planet('Other', distance=2000, velocity=5,
                                     direction=CLOCKWISE)
        other_planet = self.solar_system.planets[-1]
        polygon = self.solar_system.planets_polygon()
        assert_that(polygon, instance_of(Polygon))
        assert_that(polygon.vertices, contains(
            self.betelgeuse.location.cartesian(),
            self.aldebaran.location.cartesian(),
            other_planet.location.cartesian()))

    def test_it_knows_whether_the_planets_polygon_contains_sun(self):
        self.solar_system.add_planet('Other', distance=1000, velocity=5,
                                     direction=CLOCKWISE)
        self._set_angles(68, 204, 20)
        assert_that(
            self.solar_system.does_planets_polygon_contain_sun(), is_(True))
        self._set_angles(68, 70, 20)
        assert_that(
            self.solar_system.does_planets_polygon_contain_sun(), is_(False))

    def test_it_returns_planet_perimieter(self):
        polygon = Mock(perimeter=Mock(return_value=100))
        self.solar_system.planets_polygon = Mock(return_value=polygon)
        assert_that(self.solar_system.planets_perimeter(), equal_to(100))

    def _set_angles(self, *angles):
        for i, angle in enumerate(angles):
            self.solar_system.planets[i].location.angle = angle
